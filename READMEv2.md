## ERD: Courseme Web App
This documentation explore the design of Courseme, a courses management web app.

We'll use basic client/server architecture, Where a single server is deployed on a cloud provider next to a No-SQL database, and serving HTTP traffic from a public endpoint.

## Storage
We'll use a relational database (schema follows) to fast retrieval of Money cycle. A simple No-SQL database such as MongoDB. Data will be stored on the server on a separate, backed up volume for resilience. There will be no replication or sharding of data at this early stage.

## Scheme:
We'll need at least the following entities to implement the service:

**Student**:
| Column | Type |
|--------|------|
| ID | STRING/UUID |
| FirstName | STRING |
| LastName | STRING |
| PhoneNumber | NUMBER |
| Email | STRING |
| Password | STRING |
| avater | BUFFER |
| tokens | STRING |
| courses | [CourseID] |
| grades | [{course,grade}] |
| cart | STRING |
| CreatedAt | Timestamp |


**Instractor**:
| Column | Type |
|--------|------|
| ID | STRING/UUID |
| FirstName | STRING |
| LastName | STRING |
| PhoneNumber | NUMBER |
| Email | STRING |
| Password | STRING |
| avater | BUFFER |
| tokens | STRING |
| courses | [CourseID] |
| CreatedAt | Timestamp |

**Course**:
| Column | Type |
|--------|------|
| Id | STRING/UUID |
| Title | STRING |
| Description | STRING |
| Avatar | STRING |
| OwnerId | STRING |
| Student | ARRAY |
| Price | NUMBER |
| Reviews | NUMBER |
| Category | NUMBER |
| Content | Object |
| CreatedAt | Timestamp |

**Invoices**:
| Column | Type |
|---------|------|
| InvoiceId | STRING/UUID |
| OwnerId | STRING |
| Members | ARRAY |
| TotalBalance | NUMBER |
| Paid | NUMBER |
| Receiver | NUMBER |
| CreatedAt | Timestamp |

## Server

A simple HTTP server is responsible for authentication, serving stored data, and
potentially ingesting and serving analytics data.

- Node.js is selected for implementing the server for speed of development.
- Express.js is the web server framework.
- Sequelize to be used as an ORM.

### Auth

For v1, a simple JWT-based auth mechanism is to be used, with passwords
encrypted and stored in the database. OAuth is to be added initially or later
for Google + Facebook and maybe others (Github?).

### API

**Auth**:

```
/signIn   [POST]
/signUp   [POST]
/signOut  [POST]
```

**Users**:

```
// Creation
/users/addNewUser     [POST] ✅
/users/addFriend      [POST]
/users/addInvoice     [POST]
/users/me/avatar      [POST] ✅

// Edit
/users/edit/:id       [POST] ✅

// Search
/users/:id            [GET] ✅
/users/search/:num    [GET] ✅

// Display
/users/list           [GET] ✅
/users/me             [GET] ✅
/users/friends/:id    [GET] ✅
/users/history/:id    [GET] ✅
/users/activeInvoices [GET] ✅
/users/rate/:id       [GET] ✅

// Delete
/users/:id            [DELETE] ✅

// Operations
/users/login          [POST] ✅
/users/logout         [POST] ✅
```

**CSGs**:

```
/csgs/list  [GET]
/csgs/new   [POST]
/csgs/:id   [GET]
/csgs/:id   [DELETE]
```

**Invoices**:

```
/invoices/list  [GET]
/invoices/new   [POST]
/invoices/:id   [GET]
/invoices/:id   [DELETE]
```

## Clients

For now we'll start with a single web client, possibly adding mobile clients later.

The mobile client will be implemented in React-Native with Expo.

## Hosting

The code will be hosted on Github.
The configured to point to the web host's server public IP.

We'll deploy the server to a (likely shared) VPS for flexibility. The VM will have HTTP/HTTPS ports open, and we'll start with a manual deployment, to be automated
later using Github actions or similar. The server will have closed CORS policy except.
