const router = require("express").Router();
const userController = require("../controllers/user.controllers");
const reqAuth = require("../middlewares/reqAuth");

/*

    ✍️ View Profile
    ✍️ Get user by Id
    ✍️ Edit profile info
    ✍️ View my courses [Student]
    ✍️ View my courses  [Instructor]
    ✍️ View my grades [Student]
    ✍️ View my schedule [Student]
    ✍️ View my history [Student]

*/

router.get("/me", reqAuth, userController.myProfile);

router.post("/:id/:rule", reqAuth, userController.userById);

router.put("/edit/", reqAuth, userController.editMyProfile);

router.put("/courses/", reqAuth, userController.myCourses);

router.put("/grades/", reqAuth, userController.myGrades);

router.put("/schedule/", reqAuth, userController.mySchedule);

router.put("/history/", reqAuth, userController.myHistory);

module.exports = router;
