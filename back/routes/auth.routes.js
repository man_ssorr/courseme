const express = require("express");

const authController = require("../controllers/auth.controllers");
const reqAuth = require("../middlewares/reqAuth");

const router = express.Router();

router.post("/signup", authController.signup);

router.post("/signin", authController.signin);

router.get("/signout", reqAuth, authController.signout);

router.get("/signoutAll", reqAuth, authController.signoutAll);

router.post("/changePassword", reqAuth, authController.changePass);

module.exports = router;
