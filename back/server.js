require("./models/student.model");
require("./models/instructor.model");
require("./models/admin.model");

const express = require("express");
const cors = require("cors");

const { mongoConnect } = require("./utils/mongo");
const bodyParser = require("body-parser");

const authRoutes = require("./routes/auth.routes");

const reqAuth = require("./middlewares/reqAuth");

require("dotenv").config();

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(authRoutes);
// app.use("/student", studentRouters);
// app.use("/instructor", instructorRouters);
// app.use("/admin", adminRouters);

const PORT = process.env.PORT || 5000;

app.put("/", reqAuth, (req, res) => {
  res.send(`Hello ${req.user.rule}`);
});

async function startServer() {
  await mongoConnect();
  app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}🚀...`);
  });
}

startServer();
