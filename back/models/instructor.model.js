const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Schema = mongoose.Schema;



const instructorSchema = new Schema(
  {
    firstName: String,
    lastName: String,
    phoneNumber: {
      type: Number,
      unique: true,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Email is invalid");
        }
      },
    },
    password: {
      type: String,
      required: true,
      minlength: 8,
    },
    rule: {
      type: String,
      required: true,
    },
    tokens: [
      {
        token: {
          type: String,
          required: true,
        },
      },
    ],
    avatar: {
      type: Buffer,
    },
    courses: [
      {
        type: Schema.Types.ObjectId,
        ref: "Course",
        autopopulate: { maxDepth: 2 },
      },
    ],
  },

  {
    timestamps: true,
  }
);

// Remove the field of password and token all around the system
instructorSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();

  delete userObject.password;
  delete userObject.tokens;
  delete user.__v;

  return userObject;
};

// Generate a token by JWT
instructorSchema.methods.generateAuthToken = async function () {
  const user = this;
  const token = jwt.sign(
    { _id: user._id, rule: user.rule },
    process.env.JWT_SECRET
  );
  user.tokens = user.tokens.concat({ token });
  await user.save();
  return token;
};

// Check login process
instructorSchema.statics.findByCredentials = async (email, password) => {
  const user = await Instructor.findOne({ email });

  if (!user) {
    throw new Error("Unable to login ⛔️ !");
  }

  const isMatch = await bcrypt.compare(password, user.password);

  if (!isMatch) {
    throw new Error("Wrong password ❌ !");
  }

  return user;
};

// Hashing plain text password
instructorSchema.pre("save", async function (next) {
  const user = this;

  if (user.isModified("password")) {
    user.password = await bcrypt.hash(user.password, 8);
  }

  next();
});

const Instructor = mongoose.model("Instructor", instructorSchema);

module.exports = Instructor;
