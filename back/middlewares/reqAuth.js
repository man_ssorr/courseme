const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Student = mongoose.model("Student");
const Instructor = mongoose.model("Instructor");
const Admin = mongoose.model("Admin");

const reqAuth = async (req, res, next) => {
  try {
    const { authorization } = req.headers;

    if (!authorization) {
      throw new Error("can't find authorization header");
    }

    const token = authorization.replace("Bearer ", "");

    const { _id, rule } = jwt.verify(token, process.env.JWT_SECRET);
    console.log("🔐", rule);

    switch (rule) {
      case "Student":
        const student = await Student.findOne({
          _id,
          rule,
          "tokens.token": token,
        });
        if (!student) {
          throw new Error("Student not found");
        }
        req.user = student;

        break;
      case "Instructor":
        const instructor = await Instructor.findOne({
          _id,
          rule,
          "tokens.token": token,
        });
        if (!instructor) {
          throw new Error("Instructor not found");
        }
        req.user = instructor;
        break;
      case "Admin":
        const admin = await Admin.findOne({
          _id,
          rule,
          "tokens.token": token,
        });
        if (!admin) {
          throw new Error("Admin not found");
        }
        req.user = admin;
        break;
      default:
        throw new Error("You're not authorized to do this action 🙅");
        break;
    }
    
    req.rule = rule;
    req.token = token;

    next();
  } catch (err) {
    res.status(401).send({
      error: err.message,
      message: "reqAuth/error here!",
    });
    console.log("reqAuth/error here", err.message);
  }
};

module.exports = reqAuth;
