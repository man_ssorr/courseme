const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Schema = mongoose.Schema;
const lessonschema= new Schema(
    {   lessons:{title:String,
        resource_url: String  ,
      
        description:String
         },
  grade: {
    type: Number,
    required: true,
    default: -1,
  },
},
{
  timestamps: true,
}
    
  );

const courseSchema = new Schema(
  {
    title: String,
    description: String,
    price:{
      type:Number,
      required: true
    } ,

    avatar: {
      type: Buffer,
    },
    ownerid: [
      {
        type: Schema.Types.ObjectId,
        ref: "Instructor",
        autopopulate: { maxDepth: 2 },
      },
    ],
    student: [
      {
        type: Schema.Types.ObjectId,
        ref: "Student",
        autopopulate: { maxDepth: 2 },
      },
    ],
    published: {
        type: Boolean,
        default: false
      },
    reviews: number,
    comment:{type:String,
    }  ,
    annoncement:String,
    category:{ 
        type: String,
        required:true
         },
    chapter:{ name:String ,
             lesson:[lessonschema]
            } ,
    updated: Date
  },

  {
    timestamps: true,
  }
);

// Remove the course
//add course
//update course
//remove lesson 

//add lesson to chapter    .......
const newlesson = async (req, res) => {
    try {
      let lesson = req.body.lesson
      let name = req.body.name

      let course = await Course.find({$and:[req.course._id,req.course.chapter.name]},{$push: {lesson: req.body.lesson}, updated : Date.now()}, {new: true})
       .populate('Instructor', '_id name')
       .exec()
       res.json(result)
  
    } catch (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
  }
  //update course by add chapter and lesson 
  const updateCourse = async (req, res) => {
    try {
      let result = await Course.findByIdAndUpdate(req.course._id, {$push: {chapter: {name: req.body.name, lessons: req.body.lesson}}, 
                               updated: Date.now()}, {new: true})
                              .populate('Instructor', '_id name')
                              .exec()
      res.json(result)
    } catch (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
  }

//remove course

const removeCourse = async (req, res) => {
    try {
      let remove = await course.findByIdAndRemove(req.course.id)
      res.json(remove)
    } catch (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
  }
//remove chapter
const removeChapter= async (req, res) => {
    try {
      let result = await Course.find(req.course._id, {$pull: {chapter: {name: req.body.name, lessons: req.body.lesson}}})                 
      res.json(result)
    } catch (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
  }
  
//remove lesson
const removeLesson= async (req, res) => {
    try {
      let result = await Course.find({$and:[req.course._id,req.course.chapter.name]}, {$pull: {chapter: {lessons: req.body.lesson}}})                 
      res.json(result)
    } catch (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
  }
  


//add annonc by instructor
const addAnnoncementByinstructor = async (req, res) => {
    try {
      let result = await Course.findByIdAndUpdate(req.course._id, {$push: {annoncement:  req.body.annoncement}, 
                               updated: Date.now()}, {new: true})
                              .populate('Instructor', '_id name')
                              .exec()
      res.json(result)
    } catch (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
  }


   //add comment by student
const addCommentByStudent = async (req, res) => {
    try {
      let result = await Course.findByIdAndUpdate(req.course._id, {$push: {comment:  req.body.comment}, 
                               updated: Date.now()}, {new: true})
                              .populate('Student', '_id name')
                              .exec()
      res.json(result)
    } catch (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
  }
 
    //add comment by instructor
const replyCommentByInstructor = async (req, res) => {
    try {
      let result = await Course.findByIdAndUpdate(req.course._id, {$push: {comment:  req.body.comment}, 
                               updated: Date.now()}, {new: true})
                              .populate('Instructor', '_id name')
                              .exec()
      res.json(result)
    } catch (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
  }

  //remove comment by instructor
const removeComment= async (req, res) => {
    try {
      let result = await Course.find({$and:[req.course._id,req.course.comment]}, {$pull: {comment: req.body.comment}})                 
      res.json(result)
    } catch (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
  }


  //remove instructor

const removeInstructor = async (req, res) => {
  try {
    let remove = await course.findByIdAndRemove({ownerid: {$gte:req.body.ownerid}})
    res.json(remove)
  } catch (err) {
    return res.status(400).json({
      error: errorHandler.getErrorMessage(err)
    })
  }
}

//remove student

const removeStudent = async (req, res) => {
  try {
    let remove = await course.findOneAndDelete({student: {$gte:req.body.student}})
    res.json(remove)
  } catch (err) {
    return res.status(400).json({
      error: errorHandler.getErrorMessage(err)
    })
  }
}
const courseModel = mongoose.model("Course", courseSchema);

module.exports = courseModel;



