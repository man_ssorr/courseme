const mongoose = require("mongoose");

const Student = mongoose.model("Student");
const Instructor = mongoose.model("Instructor");
const Admin = mongoose.model("Admin");

class User {

  static signin = async (req, res) => {
    //email, password => hash
    const { email, password, rule } = req.body;
    try {
      if (!email || !password || !rule) {
        return res.status(400).send({
          error: "AuthCtrl/Must provide your email, password and rule",
        });
      }

      switch (rule) {
        case "Student":
          const student = await Student.findByCredentials(email, password);
          if (!student) {
            return res
              .status(404)
              .send({ error: "AuthCtrl/student Not Found" });
          }
          const studentToken = await student.generateAuthToken();
          const popedStudent = await Student.findById(student._id);
          res.send({ token: studentToken, user: popedStudent });
          break;
        case "Instructor":
          const instructor = await Instructor.findByCredentials(
            email,
            password
          );
          if (!instructor) {
            return res
              .status(404)
              .send({ error: "AuthCtrl/instructor Not Found" });
          }
          const instructorToken = await instructor.generateAuthToken();
          const popedInstructor = await Instructor.findById(instructor._id);
          res.send({ token: instructorToken, user: popedInstructor });
          break;
        case "Admin":
          const admin = await Admin.findByCredentials(email, password);
          if (!admin) {
            return res.status(404).send({ error: "AuthCtrl/admin Not Found" });
          }
          const adminToken = await admin.generateAuthToken();
          const popedAdmin = await Admin.findById(admin._id);
          res.send({ token: adminToken, user: popedAdmin });
          break;
        default:
          res.send("AuthCtrl/signin Rule not found");
          break;
      }
    } catch (err) {
      console.log("err", err.message);
      return res
        .status(422)
        .send({ error: err.message, message: "Error in singing" });
    }
  };

  static signup = async (req, res) => {
    const { email, password, firstName, lastName, phoneNumber, rule, avatar } =
      req.body;
    try {
      switch (rule) {
        case "Student":
          const student = await Student.create({
            email,
            password,
            firstName,
            lastName,
            phoneNumber,
            rule,
            avatar,
          });
          const studentToken = await student.generateAuthToken();
          const popedStudent = await Student.findById(student._id);
          res.status(201).send({ token: studentToken, user: popedStudent });
          break;
        case "Instructor":
          const instructor = await Instructor.create({
            email,
            password,
            firstName,
            lastName,
            phoneNumber,
            rule,
            avatar,
          });
          const instructorToken = await instructor.generateAuthToken();
          const popedInstructor = await Instructor.findById(instructor._id);
          res
            .status(201)
            .send({ token: instructorToken, user: popedInstructor });
          break;
        case "Admin":
          const admin = await Admin.create({
            email,
            password,
            firstName,
            lastName,
            phoneNumber,
            rule,
            avatar,
          });
          const adminToken = await admin.generateAuthToken();
          const popedAdmin = await Admin.findById(admin._id);
          res.status(201).send({ token: adminToken, user: popedAdmin });
          break;
        default:
          res.send("AuthCtrl/signup Rule not found");
          break;
      }
    } catch (err) {
      return res
        .status(422)
        .send({ error: err.message, message: "Error in creating user" });
    }
  };

  static signout = async (req, res) => {
    // remove token from tokens => filter
    const { user, token, rule } = req;
    console.log("user", user._id);
    try {
      switch (rule) {
        case "Student":
          const student = await Student.findById(user._id);
          student.tokens = student.tokens.filter((t) => {
            //{token:"", _id:""}   ey.....
            return t.token != token;
          });
          await student.save();
          res.status(200).send({
            message: "logged out",
          });
          break;
        case "Instructor":
          const instructor = await Instructor.findById(user._id);
          instructor.tokens = instructor.tokens.filter((t) => {
            return t.token != token;
          });
          await instructor.save();
          res.status(200).send({
            message: "logged out",
          });
          break;
        case "Admin":
          const admin = await Admin.findById(user._id);
          admin.tokens = admin.tokens.filter((t) => {
            return t.token != token;
          });
          await admin.save();
          res.status(200).send({
            message: "logged out",
          });
          break;
        default:
          res.status(500).send("AuthCtrl/signout Rule not found");
          break;
      }
    } catch (err) {
      res.status(500).send({
        errors: err.message,
        message: "error in signout",
      });
    }
  };

  static signoutAll = async (req, res) => {
    // remove all tokens req.user.tokens = []
    const { user, token, rule } = req;
    try {
      switch (rule) {
        case "Student":
          const student = await Student.findById(user._id);
          student.tokens = [];
          await student.save();
          res.status(200).send({ message: "signoutAll" });
          break;
        case "Instructor":
          const instructor = await Instructor.findById(user._id);
          instructor.tokens = [];
          await instructor.save();
          res.status(200).send({ message: "signoutAll" });
          break;
        case "Admin":
          const admin = await Admin.findById(user._id);
          admin.tokens = [];
          await admin.save();
          res.status(200).send({ message: "signoutAll" });
          break;
        default:
          res.status(500).send("AuthCtrl/signoutAll Rule not found");
          break;
      }
    } catch (err) {
      res.status(500).send({
        errors: err.message,
        message: "error in signoutAll",
      });
    }
  };

  static changePass = async (req, res) => {
    const { newPassword } = req.body;
    const { user, token, rule } = req;

    try {
      switch (rule) {
        case "Student":
          const student = await Student.findById(user._id);
          student.password = newPassword;
          await student.save();
          res.status(200).send({ message: "password changed" });
          break;
        case "Instructor":
          const instructor = await Instructor.findById(user._id);
          instructor.password = newPassword;
          await instructor.save();
          res.status(200).send({ message: "password changed" });
          break;
        case "Admin":
          const admin = await Admin.findById(user._id);
          admin.password = newPassword;
          await admin.save();
          res.status(200).send({ message: "password changed" });
          break;
        default:
          res.status(500).send("AuthCtrl/changePass Rule not found");
          break;
      }
    } catch (err) {
      res.status(500).send({
        message: "cannot change password",
      });
    }
  };
  
}

module.exports = User;
