const mongoose = require("mongoose");
const Student = mongoose.model("Student");
const Instructor = mongoose.model("Instructor");
const Admin = mongoose.model("Admin");

class User {
  static myProfile = async (req, res) => {
    const { user, rule, token } = req;
    try {
      res.send({ user, rule });
    } catch (err) {
      console.log("err", err.message);
      res.status(500).send({ error: "User/myProfile" });
    }
  };

  static userById = async (req, res) => {
    const { user, rule, token } = req;
    const { id, rule } = req.params;

    try {
      switch (rule) {
        case "Student":
          const student = await Student.findById(user._id);
          if (!student) {
            return res
              .status(401)
              .send({ message: "UserCtrl/myProfile Student not found" });
          }
          res.send(student);
          break;
        case "Instructor":
          const instructor = await Instructor.findById(user._id);
          if (!instructor) {
            return res
              .status(401)
              .send({ message: "UserCtrl/myProfile Instructor not found" });
          }
          res.send(instructor);
          break;
        case "Admin":
          res
            .status(401)
            .send({ error: "UserCtrl/myProfile Can't view Admin" });

          break;
        default:
          res.status(500).send("UserCtrl/myProfile Rule not found");
          break;
      }
    } catch (err) {
      res
        .status(404)
        .send({ message: "error in getting user by Id", error: err.message });
    }
  };

  static editMyProfile = async (req, res) => {
    const { user, rule, token } = req;
    try {
      switch (rule) {
        case "Student":
          const student = await Student.findByIdAndUpdate(user._id, req.body);
          if (!student) {
            return res
              .status(401)
              .send({ error: "UserCtrl/myProfile Can't edit Student" });
          }
          res.send(student);
          break;
        case "Instructor":
          const instructor = await Instructor.findByIdAndUpdate(
            user._id,
            req.body
          );
          if (!instructor) {
            return res
              .status(401)
              .send({ error: "UserCtrl/myProfile Can't edit Instructor" });
          }
          res.send(instructor);
          break;
        case "Admin":
          res
            .status(401)
            .send({ error: "UserCtrl/myProfile Can't edit Admin" });
          break;
        default:
          res.status(500).send("UserCtrl/myProfile Rule not found");
          break;
      }
    } catch (err) {
      res
        .status(404)
        .send({ message: "error in editing your profile", error: err.message });
    }
  };

  static myCourses = async (req, res) => {
    const { user, rule, token } = req;
    try {
      switch (rule) {
        case "Student":
          const student = await Student.findById(user._id);
          if (!student) {
            return res
              .status(401)
              .send({ message: "UserCtrl/myCourses Student not found" });
          }
          res.send(student.courses);
          break;
        case "Instructor":
          const instructor = await Instructor.findById(user._id);
          if (!instructor) {
            return res
              .status(401)
              .send({ message: "UserCtrl/myCourses Instructor not found" });
          }
          res.send(instructor.courses);
          break;
        case "Admin":
          res
            .status(401)
            .send({ error: "UserCtrl/myCourses Can't view Admin" });

          break;
        default:
          res.status(500).send("UserCtrl/myCourses Rule not found");
          break;
      }
    } catch (err) {
      res.status(404).send({
        message: "UserCtrl/myCourses User not found",
        error: err.message,
      });
    }
  };

  static myGrades = async (req, res) => {
    const { user, rule, token } = req;
    try {
      switch (rule) {
        case "Student":
          const student = await Student.findById(user._id);
          if (!student) {
            return res
              .status(401)
              .send({ message: "UserCtrl/myGrades Student not found" });
          }
          res.send(student.grades);
          break;
        case "Instructor":
          res
            .status(401)
            .send({ error: "UserCtrl/myGrades Can't view Instructor" });

          break;
        case "Admin":
          res.status(401).send({ error: "UserCtrl/myGrades Can't view Admin" });
          break;
        default:
          res.status(500).send("UserCtrl/myGrades Rule not found");
          break;
      }
    } catch (err) {
      res.status(404).send({
        message: "Can't get your grades",
        error: err.message,
      });
    }
  };

  static mySchedule;
  static myHistory;
}

module.exports = User;
