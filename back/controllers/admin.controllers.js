const userModel = mongoose.model("User");
const mongoose = require("mongoose");

class Admin {
  static getAllUsers = async (req, res) => {
    try {
      const users = await userModel.find().sort({ email: 1 });

      // there are no users
      if (!users.length) return res.status(200).json("No users here");

      // there are user(s)
      res.status(200).json({
        apiStatus: true,
        data: users,
        message: "users fetched",
      });
    } catch (e) {
      res.status(500).send({
        apiStatus: false,
        errors: e.message,
        message: "error in fetching",
      });
    }
  };

  static editUser = async (req, res, next) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        apiStatus: false,
        message: `5arbanah not a valid ID! :(`,
      });
    }

    try {
      const user = await userModel.findByIdAndUpdate(id, req.body, {
        runValidators: true,
      });

      if (!user) return res.status(500).json("User not found!");

      res.status(200).send({
        apiStatus: true,
        data: req.body,
        message: "User updated",
      });
    } catch (error) {
      res.status(304).send({
        apiStatus: false,
        errors: error.message,
        message: "User can't updated :( !",
      });
    }
  };

  static deleteUser = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        apiStatus: false,
        message: `5arbanah not a valid ID! :(`,
      });
    }

    try {
      const user = await userModel.findByIdAndRemove(id).exec();

      if (!user) return res.status(500).json("User not found!");

      res.status(200).send({
        apiStatus: true,
        data: user,
        message: "user deleted",
      });
    } catch (e) {
      res.status(500).send({
        apiStatus: false,
        errors: e.message,
        message: "error in deleting",
      });
    }
  };

  static profileImg = async (req, res) => {
    req.user.image = req.file.path;
    await req.user.save();
    res.status(200).send({
      apiStatus: true,
      data: req.file,
      message: "uploaded",
    });
  };
}

module.exports = Admin;
