import React from "react";
import ReactDOM from "react-dom";
import mainTheme from "./utils/theme";
import { MantineProvider } from "@mantine/core";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { NotificationsProvider } from "@mantine/notifications";

ReactDOM.render(
  <React.StrictMode>
    <MantineProvider withNormalizeCSS theme={mainTheme}>
      <NotificationsProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </NotificationsProvider>
    </MantineProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
