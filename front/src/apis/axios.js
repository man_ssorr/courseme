import axios from "axios";

const baseURL = `http://127.0.0.1:5000`;

export default axios.create({
  baseURL,
  timeout: 5000,
});

export function callGetUser(id = ""){
  const token = localStorage.getItem("token") || "";
  const url = `/api/user/${id}`;    
  const headers = 
  {      
      "authorization": token,      
  };       
  axios.get(url, { headers: headers })        
      .then((response) => {          
            return response.data;  
        })        
      .catch((error) => {          
            console.log(error);               
       });    
}