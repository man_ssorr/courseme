import { MantineThemeOverride } from "@mantine/core";

const mainTheme: MantineThemeOverride = {
  colorScheme: "light",

  defaultRadius: 10,

  // black: "#2B2B2B",
  // Always take the 7th one
  colors: {
    mainBlue: [
      "#C9CED8",
      "#B0B9CB",
      "#97A5C1",
      "#7D92BC",
      "#6280BB",
      "#436DBF",
      "#2D5EBE",
      "#36599E",
      "#3A5385",
      "#3B4D71",
      "#3A4761",
      "#374153",
      "#343B49",
    ],
    lightBlue: [
      "#E3EDFF",
      "#BCD1F6",
      "#A1B9E6",
      "#8DA6D3",
      "#7E95BE",
      "#7286AB",
      "#E3EDFF",
      "#687999",
    ],
  },

  primaryColor: "mainBlue",

  fontFamily: "Roboto, sans-serif",

  // fontSizes: {
  //   xs: 20,
  //   sm: 25,
  //   md: 30,
  //   lg: 35,
  //   xl: 50,
  // },

  radius: {
    xs: 10,
    sm: 15,
    md: 20,
    lg: 30,
    xl: 50,
  },

  // spacing: {
  //   xs: 30,
  //   sm: 40,
  //   md: 60,
  //   lg: 80,
  //   xl: 100,
  // },

  // shadows: {
  //   // other shadows (xs, sm, lg) will be merged from default theme
  //   md: "1px 1px 3px rgba(0,0,0,.25)",
  //   xl: "5px 5px 3px rgba(0,0,0,.25)",
  // },

  breakpoints: {
    xs: 360,
    sm: 600,
    md: 900,
    lg: 1200,
    xl: 1440,
  },

  headings: {
    fontFamily: "Roboto, sans-serif",
    sizes: {
      h1: { fontSize: 50 },
    },
  },

  other: {
    fontFamilyLogo: "Secular One",
    fontFamilySecondary: "Work Sans",
    mySpacing: [30, 40, 60, 80, 100],
    myFontSizing: [20, 25, 30, 35, 50],
  },
};

export default mainTheme;
