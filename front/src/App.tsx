import HomePage from "./pages/Home/index";
import Login from "./pages/login/Login";
import Signup from "./pages/signup/Signup";
// import { PrivateRoute } from "./pages/PrivateRouter";
// Mian Components
import { Switch, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route
          exact
          path="/"
          component={HomePage}
        />

        <Route path="/login" component={Login} />
        <Route path="/signup" component={Signup} />
      </Switch>
    </div>
  );
}

export default App;
