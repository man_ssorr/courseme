import { Text, createStyles, Button, Image, Group } from "@mantine/core";
import { NavLink } from "react-router-dom";
import LoginForm from "../../components/LoginForm";

const useStyles = createStyles((theme) => {
  const BREAKPOINT = theme.fn.smallerThan("sm");
  const HIDEBLOCK = theme.fn.smallerThan("md");
  return {
    wrapper: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      borderRadius: theme.radius.lg,
      padding: "50px",
      marginLeft: "auto",
      marginRight: "auto",
      maxWidth: "70vw",
      columnGap: "100px",
      [BREAKPOINT]: {
        flexDirection: "column",
      },
    },

    contacts: {
      display: "flex",
      borderRadius: theme.radius.md,
      backgroundColor: "#2D5EBE",
      border: "1px solid transparent",
      padding: theme.spacing.xl,
      justifyContent: "space-between",
      flex: "1 0.25 200px",
      maxWidth: "650px",
      [HIDEBLOCK]: {
        display: "none",
      },
      [BREAKPOINT]: {
        display: "flex",
        flex: "1 0.25 200px",
        marginBottom: theme.spacing.sm,
        paddingLeft: theme.spacing.md,
      },
    },

    title: {
      marginTop: theme.spacing.xl * 1.5,
      fontFamily: `Greycliff CF, ${theme.other.fontFamilySecondary}`,
      fontSize: "50px",
      [BREAKPOINT]: {
        marginBottom: theme.spacing.xl,
        fontSize: "25px",
      },
    },
  };
});

export function Login() {
  const { classes } = useStyles();

  return (
    <div className={classes.wrapper}>
      <LoginForm />
      <Group className={classes.contacts}>
        <Text
          size="lg"
          weight={700}
          className={classes.title}
          sx={{ color: "#fff" }}
        >
          Don't have an account?
        </Text>
        <NavLink
          style={{
            textDecoration: "none",
          }}
          to="/signup"
        >
          <Button variant="white"  size="lg" fullWidth radius={5}>
            Join Now
          </Button>
        </NavLink>

        <Image src="./images/EducationLogin.png" />
      </Group>
    </div>
  );
}

export default Login;
